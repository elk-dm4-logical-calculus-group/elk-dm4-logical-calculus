# Учебный лабораторный комплекс по дисциплине "Дискретная математика: логические исчисления"

## Подготовка к сборке и запуску

1. Установить [docker](https://www.docker.com/) и [docker-compose](https://docs.docker.com/compose/)
2. Создать файл `.env` и переопределить в нем следующие переменные среды (**комментарии стереть**):

```shell
DB_NAME=elk_dm4_db # бд с таким именем создается в субд при сборке 
DB_USER=elk_dm4 # Пользовтатель с таким именем создается в субд при сборке
DB_PASS=1234 # Такой пароль присваивается пользователю субд при сборке в postgres
DB_HOST=postgres # Хост, на котором развернута ваша БД, в докере - postgres
DB_PORT=5432 # Порт, прослушиваемый субд, по стандарту 5432
FRONT_DIR=/frontend # Папка с собранным фронтом в докер-томе (в ней будет лежать папка dist)
```

## Доступные команды

1. `make build` - сборка приложения (должен быть файл `.env`).
2. `make start` - запуск собранного приложения на порту 8000
3. `make stop` - остановка запущенного приложения
4. `make restart` - рестарт запущенного приложения
5. `make admin` - создание суперпользователя в запущенном приложении
6. `make danger` - полный сброс контейнеров и томов **НЕ ТОЛЬКО ДАННОГО ПРИЛОЖЕНИЯ, А ВООБЩЕ ВСЕХ**