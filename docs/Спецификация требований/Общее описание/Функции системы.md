## Функции системы

### Вывод формул

#### Описание

Данный компонент доступен всем категориям пользователей. Он предназначен для ускорения работы с большими формулами, а также для облегчения работы с теоремой дедукции. Для корректной работы с компонентом вначале задается требуемое окружение - контекст: тип исчисления и разрешенные теоремы. После этого в правое поле вводятся необходимые выражения (возможные варианты приведены в разделе "Функциональность"), а в левое поле будет производиться вывод результата обработки этих выражений.

#### Функциональность

**Требования к компоненту**:

1. Задание *контекста* ввода: исчисления, дополнительных теорем и заведомо истинных утверждений.
1. Обоснованный *вывод формулы* (проверка возможности вывода формулы по modus ponens в зависимости от текущего контекста).
1. Осуществление *замены переменных* в известной в контексте формуле.
1. Проверка корректности и исполнение записанного *modus ponens*.
1. Отображение *теоремы*, имеющейся в контексте, по её краткому обозначению.

**Элементы**, присутствующие на странице:

1. Элемент, поддерживающий задание начального контекста. Имеет поддержку $`\LaTeX`$.
1. *Левое* текстовое поле с поддержкой $`\LaTeX`$. Служит для более подробного раскрытия и демонстрации формул, не предназначено для ввода.
1. *Правое* текстовое поле с поддержкой $`\LaTeX`$, предназначено для записи *обоснования*. Может использоваться как активное поле в следующих ситуациях:
    * Если записана замена переменных, в левое поле записывается требуемый вид.
    * Если записан modus ponens, то проверяется его корректность и результат вписывается в левое поле.
    * Если записано обозначение некоторой теоремы, имеющейся в контексте, то в левое поле записывается эта теорема.
    * Если записано выражение, которое невозможно корректно обработать, в левом поле выводится сообщение об ошибке.

### Авторизация и аутентификация

#### Описание

Данный компонент осуществляет авторизацию и аутентификацию пользователей в системе.

#### Функциональность

**Требования к аутентификации**:

1. Аутентификация пользователей должна производиться через логин и пароль.
1. При отсутствии действий со стороны пользователя в течение более чем 30 дней необходимо попросить пользователя ввести логин и пароль повторнo.
1. Созданием пользователей занимаются администраторы. Пользователю ставится автоматически сгенерированный логин (транслитерация имени и фамилии) и пароль.
1. Необходима возможность изменения пароля путем введения старого и нового паролей.

**Требования к авторизации**:

Доступ к системе осуществляется на основе ролей. Необходимы три роли:

* Администратор
* Преподаватель
* Студент

Права каждой из ролей устанавливаются в разделах, описывающих другие компоненты системы.

**Элементы**, присутствующие на странице авторизации:

* Поля для ввода логина и пароля.
* Кнопка для входа в систему.

**Валидация**:

* Если не заполнено одно из полей (для ввода логина и пароля), то при нажатии на кнопку входа выводить на экран предупреждение о том, что не заполнено требуемое поле, и не осуществлять вход в систему.
* Если в одно из полей (для ввода логина и пароля) введено некорректное значение, то выводить на экран предупреждение о том, что поле заполнено некорректно, и не осуществлять вход в систему.

<!-- ### Личный кабинет

#### Описание

Личный кабинет - раздел сайта, который предоставляет необходимую информацию для студентов, преподавателя, администратора сайта.

* В личном кабинете *Студента* содержится информация только по его контрольным и домашним работам, а также оценки, выставленные преподавателем.
* В личном кабинете *Преподавателя* содержится информация по учебным группам: план домашних заданий и контрольных, а также оценки всех студентов группы.
* В личном кабинете *Администратора* можно делать SQL-запросы к базе данных и просматривать логи системы.

#### Функциональность

**Требования к личному кабинету** *для преподавателей*:

1. Должен представлять собой страницу с таблицей со следующими полями:
	* ФИО студента
	* План домашних заданий
	* План контрольных работ
	* Полусеместровая оценка
	* Оценка за семестр
	* Оценка за экзамен
	* Итоговая оценка

2. На странице отображается таблица только одной учебной группы. При выборе значения в выпадающем списке с номерами групп открывается таблица выбранной группы.

**Требования к личному кабинету** *для студентов*:

Должен представлять собой страницу с таблицей со следующими полями:

* Название работы (домашняя или контрольная)
* Статус: выполнено/не выполнено
* Ссылка на страницу с работой
* Дата выполнения
* Оценка

**Требования к личному кабинету** *для администраторов сайта*:

Должен представлять собой страницу, на которой будет:

* Поле для ввода SQL-запросов к базе данных
* Блок с логами системы -->

<!-- ### Обработка домашних заданий

#### Описание

Данный компонент состоит из двух частей.

* К первой из них имеют доступ только *преподаватели*. Она предназначена для выдачи домашних заданий учебным группам. 
* Вторая часть компонента доступна только *студентам*. С ее помощью они могут просматривать выданные им домашние задания и высылать выполненные домашние задания на проверку своему преподавателю.

#### Функциональность

**Требования к компоненту** для *преподавателей*:

1. Создание домашних заданий.
1. Выдача домашнего задания определённой учебной группе студентов к определённой дате.
1. Проверка выданных домашних заданий (возможна после того, как студент пришлёт выполненное домашнее задание).
1. Комментирование загруженного домашнего задания, после его отправки на проверку.
1. Выставление оценки за домашнее задание.

**Требования к компоненту** для *студентов*:

1. Просмотр всех выданных учебной группе домашних заданий с начала учебного года.
1. Возможность отправки выполненных заданий на проверку. Если студент загружает не до конца выполненное задание, оно отправляется на сервер. При повторном входе на страницу с данным заданием весь сохраненный результат возвращается пользователю.
1. Возможность просмотра результата проверки домашнего задания с комментариями преподавателя.
1. Возможность просмотра выставленных за домашние задания оценок.

**Элементы**, присутствующие на странице для *выдачи* домашнего задания:

1. Текстовое поле, поддерживающее LaTeX, содержащее текст задания.
1. Кнопка для загрузки файлов.
1. Текстовое поле для указания комментариев к выполнению задания.
1. Выпадающий список с номерами групп.
1. Поле для ввода даты сдачи домашнего задания.
1. Кнопка для рассылки заданий.

**Элементы**, присутствующие на странице для *сдачи* домашнего задания:

1. Текстовое поле, поддерживающее LaTeX, содержащее выполненное домашнее задание.
1. Кнопка для загрузки файлов.
1. Текстовое поле, содержащее комментарии преподавателя.
1. Кнопка для отправки задания на проверку.
1. Текстовое поле, содержащее выставленную оценку.

**Ограничения**:

* Размер файла не должен превышать 5Мб.
* Загружаемые файлы могут иметь формат .pdf, .txt, .docx.
  
**Валидация**:

* Если не выбрана группа в выпадающем списке или не проставлена дата сдачи задания, то при нажатии на кнопку рассылки задания выводить на экран предупреждение о том, что требуемое поле не заполнено, и не осуществлять рассылку.
* Если введена дата сдачи задания, предшествующая сегодняшней, то при нажатии на кнопку рассылки задания выводить на экран предупреждение о том, что требуемое поле заполнено некорректно, и не осуществлять рассылку. -->

<!-- ### Проверка истинности формул

Компонент предназначен для проверки истинности формул одного из логических исчислений. Доступен для всех типов пользователей. Всем пользователям предоставляется возможность:

1. выбора логического исчисления; 
2. ввода логической формулы на $\LaTeX$;
3. проверки введенной формулы на истинность.

Общие функции системы:

1. проверять введенную пользователем формулу при помощи главной интерпретации исчисления:
    * для $P_1$, $P_2$ - обычная матлогика;
    * для $F_1$, $F_2$ - метод резолюций;
    * для $P_I$​ -  трёхзначная псеводобулева алгебра;
    * для модального исчисления - некоторая модель Крипке;
2. выводить на экран результат в виде текстовой строки со значениями *Истинна* и *Не истинна*.

Если в поле ввода формулы записано выражение, которое невозможно корректно обработать, то выводится на экран уведомление об ошибке. -->