## Сертификационный базис

Сертифицирующий орган, консультируясь с Заявителем, устанавливает сертификационный базис для ПО.

В сертификационном базисе определяются конкретные пункты нормативных документов и любые специальные условия, которые
могут дополнять опубликованные нормативные документы.

Для модифицируемых модулей ПО сертифицирующий орган рассматривает влияние модификации на первоначально установленный для ПО сертификационный базис.

В некоторых случаях сертификационный базис для модифицированного модуля может не измениться по сравнению с
первоначальным, однако, первоначальные методы определения соответствия модифицированного изделия сертификационному
базису могут оказаться не применимыми и может потребоваться изменение методов определения соответствия.
