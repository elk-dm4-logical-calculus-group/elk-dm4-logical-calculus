### Макет интерфейса

* **Service.Inference.InterfaceLayout.General**:

    ![FormulaDerivation.InterfaceLayout.General](../../images/FormulaDerivation.jpg){width=0.99\linewidth}

* **Service.Inference.InterfaceLayout.Common**:
    Общий макет страницы выглядит следующим образом:

    ![FormulaDerivation.InterfaceLayout.Common](../../images/FormulaDerivationNormal.jpg){width=0.99\linewidth}

* **Service.Inference.InterfaceLayout.AddRaw**:
    Добавление соответственных строчек в блоках демонстрации формул и записи обоснования решения. (Данный сценарий реализуется нажатием на знак-кнопку "Плюс" под существующими строчками)

    ![FormulaDerivation.InterfaceLayout.AddRaw](../../images/FormulaDerivationAddRow.jpg){width=0.99\linewidth}

* **Service.Inference.InterfaceLayout.DeleteRaw**:
    Удаление соответственных строчек в блоках демонстрации формул и записи обоснования решения. (Данный сценарий реализуется нажатием на знак-кнопку "Минус" справа удаляемой строки)

    ![FormulaDerivation.InterfaceLayout.DeleteRaw](../../images/FormulaDerivationDeleteRow.jpg){width=0.99\linewidth}

* **Service.Inference.InterfaceLayout.ChooseContext**:
    Выбор контекста. (Данный сценарий реализуется раскрытием соответствующего dropdown, выбором необходимого типа исчисления, путем нажатия на него)

    ![FormulaDerivation.InterfaceLayout.ChooseContext](../../images/FormulaDerivationChooseContextOpenDropdown.jpg){width=0.99\linewidth}

    ![FormulaDerivation.InterfaceLayout.ChooseContext](../../images/FormulaDerivationChooseContextClickContext.jpg){width=0.99\linewidth}

    ![FormulaDerivation.InterfaceLayout.ChooseContext](../../images/FormulaDerivationChooseContext.jpg){width=0.99\linewidth}

* **Service.Inference.InterfaceLayout.AddNewTheorem**:
    Добавление новой разрешённой теоремы к теоремам контекста. (Данный сценарий реализуется нажатием на знак-кнопку "Плюс" под существующими разрешёнными теоремами и записью новой теоремы в появившееся поле)

    ![FormulaDerivation.InterfaceLayout.AddNewTheorem](../../images/FormulaDerivationAddNewTheoremWriteTheorem.jpg){width=0.99\linewidth}

    ![FormulaDerivation.InterfaceLayout.AddNewTheorem](../../images/FormulaDerivationAddNewTheorem.jpg){width=0.99\linewidth}

* **Service.Inference.InterfaceLayout.DeleteTheorem**:
    Удаление существующих разрешённых теорем контекста. (Данный сценарий реализуется путём нажатия на теоремы, необходимые к удалению, затем нажанием на знак-кнопку "Корзина" в правом нижнем блоке разрешённых компонент)

    ![FormulaDerivation.InterfaceLayout.DeleteTheorem](../../images/FormulaDerivationDeleteTheoremChooseTheorem.jpg){width=0.99\linewidth}

    ![FormulaDerivation.InterfaceLayout.DeleteTheorem](../../images/FormulaDerivationDeleteTheorem.jpg){width=0.99\linewidth}