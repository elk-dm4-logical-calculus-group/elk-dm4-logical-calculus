# Описание API компонента вывода формул

- **Service.Inference.API**:

|URL|Метод HTTP|Тип тела запроса|Тип ответа|Назначение|
|---|----------|----------------|----------|----------|
| ​/api​/calculus​/ | GET | *(none)* | 200: application/json: `[{"id":0,"inference_rules":[{"id":0,"name":"string","notation":"string","is_axiom":true}],"name":"string"}]` | Список исчислений |
| ​/api​/calculus​/{id}​/ | GET | *(none)* | 200: application/json: `{"id":0,"inference_rules":[{"id":0,"name":"string","notation":"string","is_axiom":true}],"name":"string"}` | Данные конкретного исчисления с аксиомами и правилами вывода |
| ​/api​/core​/make_inference​/ | POST | application/json: `{"command":"string","axioms":[{"name":"string","notation":"string"}],"rules":[{"name":"string","notation":"string"}],"steps":[{"formula":"string","command":"string"}]}` | 200: application/json: `{"formula":"string"}` | Запуск вывода формулы на основе аксиом и правил |

<end>