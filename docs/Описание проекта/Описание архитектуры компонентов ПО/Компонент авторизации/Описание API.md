# Описание API компонента авторизации

- **Service.Auth.API**:

|URL|Метод HTTP|Тип тела запроса|Тип ответа|Назначение|
|---|----------|----------------|----------|----------|
| ​/api​/users​/ | GET | | 200: application/json: `[{"username":"string","last_login":"2021-06-02T14:12:01.500Z","first_name":"string","last_name":"string","email":"user@example.com","date_joined":"2021-06-02T14:12:01.500Z"}]` | Список пользователей |
| ​/api​/users​/{id}​/ | GET | | 200: application/json: `{"username":"string","last_login":"2021-06-02T14:12:01.504Z","first_name":"string","last_name":"string","email":"user@example.com","date_joined":"2021-06-02T14:12:01.504Z"}` | Данные конкретного пользователя |
| ​/api​/users​/sign_in​/ | POST | application/json: `{"username":"string","password":"string"}` | application/json: 200: `{"token":"string"}` | Авторизация по логину/паролю и получение токена (ключа доступа) |

<end>