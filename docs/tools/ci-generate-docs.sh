#!/bin/bash
python3 docs/tools/md-tex-pdf-tool/main.py "docs/Планирование/План разработки" && cp "./docs/Планирование/План разработки/.build/template.pdf" "План разработки.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Планирование/План управления конфигурацией" && cp "./docs/Планирование/План управления конфигурацией/.build/template.pdf" "План управления конфигурацией.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Планирование/План верификации" && cp "./docs/Планирование/План верификации/.build/template.pdf" "План верификации.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Планирование/Стандарты" && cp "./docs/Планирование/Стандарты/.build/template.pdf" "Стандарты.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Спецификация требований" && cp "./docs/Спецификация требований/.build/template.pdf" "Спецификация требований.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Описание проекта" && cp "./docs/Описание проекта/.build/template.pdf" "Описание проекта.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Планирование/План сертификации" && cp "./docs/Планирование/План сертификации/.build/template.pdf" "План сертификации.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Технический регламент" && cp "./docs/Технический регламент/.build/template.pdf" "Технический регламент.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Мероприятия сертификации" && cp "./docs/Мероприятия сертификации/.build/template.pdf" "Мероприятия сертификации.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Исходный код" && cp "./docs/Исходный код/.build/template.pdf" "Исходный код.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Итоговое заключение" && cp "./docs/Итоговое заключение/.build/template.pdf" "Итоговое заключение.pdf"

python3 docs/tools/md-tex-pdf-tool/main.py "docs/Каталог комплектации" && cp "./docs/Каталог комплектации/.build/template.pdf" "Каталог комплектации.pdf"