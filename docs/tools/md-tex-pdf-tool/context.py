class Context:
    def __init__(self, **kwargs):
        self.__context__ = dict(kwargs)

    def get(self, key):
        return self.__context__[key]

