import re
import os
from typing import List, Dict
from context import Context
import subprocess

def read_root_file(ctx: Context, config: Dict[str, str]) -> List[str]:
    project_path = ctx.get("project_path")
    root_file_name = config["root"]
    root_file_path = os.path.join(project_path, root_file_name)
    with open(root_file_path, 'r', encoding="utf-8") as root_file:
        content = root_file.readlines()
    return content

def resolve_imports(ctx: Context, content_lines: List[str]) -> List[str]:
    project_path = ctx.get("project_path")
    result_lines = []
    def rec(lines: List[str]):
        for line in lines:
            do_append = True
            if (line.startswith("@import")):
                path = line.split("@import")[1].strip(" \n\"")
                import_path = os.path.join(project_path, path)
                rec(map(lambda x: x+"\n", subprocess.run(["cat", import_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode('utf-8').split("\n")))
                do_append = False
            if do_append:
                result_lines.append(line)
    rec(content_lines)
    return result_lines

def fix_markdown_for_pandoc(ctx: Context, content: List[str]) -> List[str]:
    project_path = ctx.get("project_path")
    fixed_content = []
    is_math_env = False
    is_code_env = False
    for line in content:
        fixed = line.replace("$`", "$").replace("`$", "$")
        fixed = re.sub(r"\[\^.*\]", "", fixed)
        if re.search(r"```\w+$", fixed):
            is_code_env = True
        if re.search(r"```$", fixed) and is_code_env:
            is_code_env = False
        if re.search(r"```math$", fixed):
            is_math_env = True
            fixed = fixed.replace("```math", "\\begin{equation*}")
        if re.search(r"```$", fixed) and is_math_env:
            fixed = fixed.replace("```", "\\end{equation*}")
            is_math_env = False
        img_search_group_starts = re.search(r"\!\[(.*)\]\((.*)\)\{(.*)\}", fixed)
        img_search_group = re.search(r"^\!\[(.*)\]\((.*)\)\{(.*)\}", fixed)
        if img_search_group:
            img_label = None
            img_caption = img_search_group.group(1)
            img_path_source = img_search_group.group(2)
            img_path = os.path.join(project_path, img_path_source)
            img_props_source = img_search_group.group(3)
            img_props_list = img_props_source.split(",")
            img_props = ""
            for img_prop in img_props_list:
                parts = img_prop.split("=")
                key = parts[0]
                value = parts[1]
                if key == "label":
                    img_label = value
                elif key == "width":
                    img_props += key + "=" + value
            img_template = """\\begin{{figure}}[ht]
    \\begin{{center}}
        \\includegraphics[{}]{{{}}}
    \\end{{center}}
    \\caption{{{}}}
    \\label{{{}}}
\\end{{figure}}
""".format(img_props, img_path, img_caption, img_label)
            fixed = img_template
        elif img_search_group_starts:
            if is_code_env:
                continue
            img_caption = img_search_group_starts.group(1)
            img_path_source = img_search_group_starts.group(2)
            img_path = os.path.join(project_path, img_path_source)
            img_props_source = img_search_group_starts.group(3)
            img_props_list = img_props_source.split(",")
            img_props = ""
            for img_prop in img_props_list:
                parts = img_prop.split("=")
                key = parts[0]
                value = parts[1]
                if key == "width":
                    img_props += key + "=" + value
            img_template = "\\\\includegraphics[{}]{{{}}}".format(img_props, img_path)
            fixed = re.sub(r"!\[.*]\(.*\){.*}", img_template, fixed)
        fixed_content.append(fixed)
    return fixed_content

def convert_to_tex_by_pandoc(ctx: Context, content_lines: List[str]) -> List[str]:
    project_path = ctx.get("project_path")
    build_dir_name = ".build"
    build_dir_path = os.path.join(project_path, build_dir_name)
    if not os.path.exists(build_dir_path):
        os.mkdir(build_dir_path)
    markdown_file_name = "content.md"
    markdown_file_path = os.path.join(build_dir_path, markdown_file_name)
    with open(markdown_file_path, "w", encoding="utf-8") as markdown_file:
        markdown_file.writelines(content_lines)
    tex_file_name = "content.tex"
    tex_file_path = os.path.join(build_dir_path, tex_file_name)
    os.system("pandoc '{}' -o '{}'".format(markdown_file_path, tex_file_path))
    with open(tex_file_path, 'r', encoding="utf-8") as tex_file:
        tex_content_lines = tex_file.readlines()
    return tex_content_lines

def edit_generated_tex(ctx: Context, tex_content_lines: List[str]) -> List[str]:
    for i in range(0, len(tex_content_lines)):
        line = tex_content_lines[i]
        line = line.replace("\\section", "\\chapter")
        line = line.replace("\\subsection", "\\section")
        line = line.replace("\\subsubsection", "\\subsection")
        line = line.replace("\\paragraph", "\\subsubsection")
        tex_content_lines[i] = line
    return tex_content_lines

def convert_to_pdf_by_latexmk(ctx: Context, tex_content_lines: List[str]) -> str:
    project_path = ctx.get("project_path")
    build_dir_name = ".build";
    build_dir_path = os.path.join(project_path, build_dir_name)
    tex_file_name = "content.tex"
    tex_file_path = os.path.join(build_dir_path, tex_file_name)
    with open(tex_file_path, 'w', encoding="utf-8") as tex_file:
        tex_file.writelines(tex_content_lines)
    title = ctx.get("config")["title"]
    template = """\\documentclass[12pt,a4paper,oneside,final]{{report}}
\\usepackage{{docs/tools/md-tex-pdf-tool/styles/title}}
\\input{{docs/tools/md-tex-pdf-tool/styles/preamble}}
\\begin{{document}}
    \\tableofcontents
    \\newpage
    \\input{{{}/.build/content}}
\\end{{document}}
    """.format(project_path)
    template_file_name = "template.tex"
    template_file_path = os.path.join(build_dir_path, template_file_name)
    with open(template_file_path, "w", encoding="utf-8") as template_file:
        template_file.write(template)
    os.system("latexmk -xelatex -synctex=1 -interaction=nonstopmode -file-line-error '-outdir={}' '{}'".format(build_dir_path, template_file_path))
    return "OK"