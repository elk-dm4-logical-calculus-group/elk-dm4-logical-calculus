import sys
import yaml
import os
from pipeline import Pipeline
from context import Context
import processors

CONFIG_FILENAME = ".md-project.yml"

if (len(sys.argv) != 2):
    print("Pass path to dir with document")
    exit(1)

project_path = sys.argv[1]
with open(os.path.join(project_path, CONFIG_FILENAME), 'r', encoding="utf-8") as config_file:
    config = yaml.safe_load(config_file)
context = Context(project_path=project_path, config=config)
pipeline = Pipeline(
    context,
    processors.read_root_file,
    processors.resolve_imports,
    processors.fix_markdown_for_pandoc,
    processors.convert_to_tex_by_pandoc,
    processors.edit_generated_tex,
    processors.convert_to_pdf_by_latexmk
)
result = pipeline.execute(config)
print("".join(result))