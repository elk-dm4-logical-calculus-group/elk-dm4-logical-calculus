from context import Context

class Pipeline: 
    def __init__(self, context: Context, *processors):
        self.processors = [p for p in processors]
        self.context = context

    def execute(self, initial):
        result = initial
        for processor in self.processors:
            result = processor(self.context, result)
        return result