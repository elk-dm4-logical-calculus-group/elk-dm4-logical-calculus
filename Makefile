start:
	docker-compose up -d
	docker-compose exec backend python manage.py spectacular --file ./schema/swagger.yaml
	docker-compose exec frontend npm cache clean --force
	docker-compose exec frontend npm run swagger:prod
	docker-compose exec frontend npm run build
	docker-compose exec backend python manage.py makemigrations
	docker-compose exec backend python manage.py migrate
	docker-compose exec backend python manage.py collectstatic --no-input
stop:
	docker-compose down
build:
	docker-compose down
	docker volume rm -f $$(docker volume ls | grep frontend | awk '{print $$2}')
	docker-compose build
restart:
	docker-compose down
	docker-compose up -d
	docker-compose exec backend python manage.py spectacular --file ./schema/swagger.yaml
	docker-compose exec frontend npm cache clean --force
	docker-compose exec frontend npm run swagger:prod
	docker-compose exec frontend npm run build
	docker-compose exec backend python manage.py makemigrations
	docker-compose exec backend python manage.py migrate
	docker-compose exec backend python manage.py collectstatic --no-input
admin:
	docker-compose exec backend python manage.py createsuperuser
danger:
	docker-compose down
	docker volume prune
	docker system prune -a
