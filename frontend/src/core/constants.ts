export class Constants {
    static readonly BACKEND_URL = 'http://localhost:10001'
    static readonly SESSION_TOKEN_NAME_IN_LOCAL_STORAGE = 'session_token'
}
