import Vue from 'vue'

class NotifyBuilder {
    success (title: string, text: string) {
        Vue.notify({
            type: 'success',
            title,
            text,
            duration: 5000
        })
    }

    warn (title: string, text: string) {
        Vue.notify({
            type: 'warn',
            title,
            text,
            duration: 5000
        })
    }

    error (title: string, text: string) {
        Vue.notify({
            type: 'error',
            title,
            text,
            duration: -1
        })
    }
}

export function notify () {
    return new NotifyBuilder()
}

export function wrapError (title: string, e: any) {
    console.log(e)
    if (e.error.errors) {
        console.error(e)
        notify().error(title, 'Произошла внутренняя ошибка приложения')
    } else {
        notify().error(title, e.error)
    }
}
