import { ApiConfig, HttpClient } from '@/api/http-client'
import { Constants } from '@/core/constants'
import { Store } from 'vuex'
import { storageModule } from '@/store/modules/storage'

export type RequestFunction = <T extends HttpClient<unknown>>(THttpClient: { new (apiConfig: ApiConfig<unknown>): T }) => T

export function requestWith (_store: Store<unknown>): RequestFunction {
    return function <T extends HttpClient> (THttpClient: new(apiConfig: ApiConfig) => T) {
        const sessionToken = storageModule.context(_store).state.sessionToken
        return new THttpClient({
            baseUrl: Constants.BACKEND_URL,
            baseApiParams: {
                headers: {
                    Authorization: sessionToken
                }
            }
        })
    }
}
