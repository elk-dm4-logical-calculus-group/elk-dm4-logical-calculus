import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import vuetify from './plugins/vuetify'
import { Vue2Storage } from 'vue2-storage'
import Notifications from 'vue-notification'

Vue.config.productionTip = false
Vue.use(Vue2Storage, {
    prefix: 'rwms_',
    driver: 'local',
    ttl: 60 * 60 * 24 * 1000 // 24 hour
})

Vue.use(Notifications)

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')
