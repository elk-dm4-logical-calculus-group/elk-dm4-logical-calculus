/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { User } from "./data-contracts";
import { HttpClient, RequestParams } from "./http-client";

export class Id<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
  /**
   * No description
   *
   * @tags users
   * @name UsersRead
   * @request GET:/users/{id}/
   * @secure
   * @response `200` `User`
   */
  usersRead = (id: number, params: RequestParams = {}) =>
    this.request<User, any>({
      path: `/users/${id}/`,
      method: "GET",
      secure: true,
      format: "json",
      ...params,
    });
  /**
   * No description
   *
   * @tags users
   * @name UsersUpdate
   * @request PUT:/users/{id}/
   * @secure
   * @response `200` `User`
   */
  usersUpdate = (id: number, data: User, params: RequestParams = {}) =>
    this.request<User, any>({
      path: `/users/${id}/`,
      method: "PUT",
      body: data,
      secure: true,
      format: "json",
      ...params,
    });
  /**
   * No description
   *
   * @tags users
   * @name UsersPartialUpdate
   * @request PATCH:/users/{id}/
   * @secure
   * @response `200` `User`
   */
  usersPartialUpdate = (id: number, data: User, params: RequestParams = {}) =>
    this.request<User, any>({
      path: `/users/${id}/`,
      method: "PATCH",
      body: data,
      secure: true,
      format: "json",
      ...params,
    });
  /**
   * No description
   *
   * @tags users
   * @name UsersDelete
   * @request DELETE:/users/{id}/
   * @secure
   * @response `204` `void`
   */
  usersDelete = (id: number, params: RequestParams = {}) =>
    this.request<void, any>({
      path: `/users/${id}/`,
      method: "DELETE",
      secure: true,
      ...params,
    });
}
