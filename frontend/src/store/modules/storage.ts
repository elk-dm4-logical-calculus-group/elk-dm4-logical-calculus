import { Getters, Mutations, Actions, Module } from 'vuex-smart-module'
import { Vue2Storage } from 'vue2-storage'
import { Constants } from '@/core/constants'

class StorageState {
    sessionToken = 'no-token'
    storage: Vue2Storage | null = null
}

class StorageGetters extends Getters<StorageState> {
    get isSessionTokenAvailable (): boolean {
        return this.state.sessionToken !== 'no-token'
    }
}

class StorageMutations extends Mutations<StorageState> {
    setSessionToken (token: string) {
        this.state.sessionToken = token
    }

    removeSessionToken () {
        this.state.sessionToken = 'no-token'
    }

    setStorage (payload: { storage: Vue2Storage }) {
        this.state.storage = payload.storage
    }
}

class StorageActions extends Actions<StorageState, StorageGetters, StorageMutations, StorageActions> {
    /**
     * @returns `true` if session initialized, `false` if there is no session token in storage
     */
    async initStorageModule (payload: { storage: Vue2Storage }): Promise<boolean> {
        this.mutations.setStorage({ storage: payload.storage })
        if (this.getters.isSessionTokenAvailable) {
            return true
        } else {
            const sessionToken = this.state.storage?.get(Constants.SESSION_TOKEN_NAME_IN_LOCAL_STORAGE)
            if (sessionToken == null) {
                return false
            }
            this.mutations.setSessionToken(sessionToken)
            return true
        }
    }

    async setupSessionToken (payload: { token: string }) {
        const _ = this.state.storage?.set(Constants.SESSION_TOKEN_NAME_IN_LOCAL_STORAGE, payload.token)
        this.mutations.setSessionToken(payload.token)
    }

    async dropSessionToken () {
        const _ = this.state.storage?.remove(Constants.SESSION_TOKEN_NAME_IN_LOCAL_STORAGE)
        this.mutations.removeSessionToken()
    }
}

export const storageModule = new Module({
    state: StorageState,
    getters: StorageGetters,
    mutations: StorageMutations,
    actions: StorageActions,
    namespaced: true
})
