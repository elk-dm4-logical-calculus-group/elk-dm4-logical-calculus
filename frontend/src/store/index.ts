import Vue from 'vue'
import Vuex from 'vuex'
import { createStore } from 'vuex-smart-module'
import { globalModule } from '@/store/global'

Vue.use(Vuex)

const store = createStore(
    globalModule,
    {
        strict: process.env.NODE_ENV !== 'production'
    }
)

export default store
