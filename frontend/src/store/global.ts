import { Getters, Mutations, Actions, Module } from 'vuex-smart-module'
import { storageModule } from '@/store/modules/storage'

class GlobalState {
}

class GlobalGetters extends Getters<GlobalState> {
}

class GlobalMutations extends Mutations<GlobalState> {
}

class GlobalActions extends Actions<GlobalState, GlobalGetters, GlobalMutations, GlobalActions> {
}

export const globalModule = new Module({
    state: GlobalState,
    getters: GlobalGetters,
    mutations: GlobalMutations,
    actions: GlobalActions,
    modules: {
        storage: storageModule
    }
})
