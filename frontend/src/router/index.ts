import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import { Component } from 'vue-property-decorator'

Vue.use(VueRouter)

Component.registerHooks([
    'beforeRouteEnter',
    'beforeRouteUpdate',
    'beforeRouteLeave'
])

const routes: Array<RouteConfig> = [
    {
        path: '/home',
        name: 'Home',
        redirect: '/formula-inference',
        component: () => import('@/views/Home.vue')
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/Login.vue')
    },
    {
        path: '/formula-inference',
        name: 'FormulaInference',
        component: () => import('@/views/FormulaInference.vue')
    },
    {
        path: '/formula-validity',
        name: 'FormulaValidity',
        component: () => import('@/views/FormulaValidity.vue')
    },
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '*',
        redirect: '/'
    }
]

const router = new VueRouter({
    routes
})

export default router
