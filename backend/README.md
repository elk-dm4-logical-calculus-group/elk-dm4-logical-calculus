# elk_dm4_backend

## Локальный запуск

1. Установить Python >= 3.8, pip
2. Установить постгрес, создать БД и пользователя
3. Установить глобально Pipenv `pip install pipenv`
4. В директории backend-а выполнить:

```shell
pipenv install
pipenv install --dev
```

5. Активировать виртуальное окружение: `pipenv shell`
6. Создать файл `.env` (или скопировать из [родительской папки](../), если создавали его там) и переопределить в нем следующие переменные среды (**комментарии стереть**):

```shell
DB_NAME=elk_dm4_db # Имя базы данных в postgress
DB_USER=elk_dm4 # Пользовтатель БД для доступа ORM
DB_PASS=1234 # Пароль пользователя БД
DB_HOST=127.0.0.1 # Хост, на котором развернута ваша БД, в нашем случае localhost, в докере - postgres
DB_PORT=5432 # Порт, прослушиваемый субд, по стандарту 5432
FRONT_DIR=../frontend # Папка с собранным фронтом (в ней будет лежать папка dist)
```

7. Выполнить миграции

```shell
# ./manage.py flush # - очистка бд
./manage.py makemigrations
./manage.py migrate
```

8. Выполнить в папке [frontend](../frontend/)

```shell
npm run swagger
npm run build
```

9. Можно запускать сервер `./manage.py runserver`

---
**UPD**

Swagger URL: `/api/swagger/`

Генерация Swagger-файла (yaml) делается командой

```shell
./manage.py spectacular --file swagger.yaml
```
