import pytest
from core.parsers.formula_parser import FormulaParser
from core.parsers.command_parser import CommandParser
from core.logic.calculus import Calculus
from core.logic.inference import Proof

formula_parser = FormulaParser()

p1calculus = Calculus(
    {
        "K": formula_parser.parse("|- p > (q > p)"),
        "S": formula_parser.parse("|- s > (p > q) > (s > p > (s > q))"),
        "En": formula_parser.parse("|- p > f > f > p")
    },
    {
        "MP": formula_parser.parse("A, A > B |- B")
    }
)
command_parser = CommandParser(p1calculus)


@pytest.mark.parametrize(
    ["command", "result", "proof_steps"],
    [
        ["K", "|- p > (q > p)", []],
        ["[p/q]K", "|- p > (p > p)", []],
        ["MP([p/q]K,[p/q, p/s]S)", "|- p > p > (p > p)", []]
    ]
)
def test_inference(command, result, proof_steps):
    proof = Proof(p1calculus, proof_steps)
    f = proof.apply(command_parser.parse(command))
    assert str(f) == result


def test_inference_of_comb_i():
    steps = []

    def step(cmd: str):
        steps.append(Proof(p1calculus, steps).apply(command_parser.parse(cmd)))

    step("S")
    assert str(steps[0]) == "|- s > (p > q) > (s > p > (s > q))"
    step("[q/p,p/q,p/s]1")
    assert str(steps[1]) == "|- p > (q > p) > (p > q > (p > p))"
    step("K")
    assert str(steps[2]) == "|- p > (q > p)"
    step("MP(3,2)")
    assert str(steps[3]) == "|- p > q > (p > p)"
    step("[q>p/q]4")
    assert str(steps[4]) == "|- p > (q > p) > (p > p)"
    step("MP(3,5)")
    assert str(steps[5]) == "|- p > p"


def test_inference_of_f():
    steps = []

    def step(cmd: str):
        steps.append(Proof(p1calculus, steps).apply(command_parser.parse(cmd)))

    step("[p>f>f/p,p/q,f/s]S")
    assert str(steps[0]) == "|- f > (p > f > f > p) > (f > (p > f > f) > (f > p))"

