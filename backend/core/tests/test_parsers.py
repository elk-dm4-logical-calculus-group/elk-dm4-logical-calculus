import pytest
from core.parsers.formula_parser import FormulaParser
from core.parsers.command_parser import CommandParser
from core.logic.calculus import Calculus

formula_parser = FormulaParser()

p1calculus = Calculus(
    {
        "K": formula_parser.parse("|- p > (q > p)"),
        "S": formula_parser.parse("|- s > (p > q) > (s > p > (s > q))"),
        "En": formula_parser.parse("|- p > f > f > p")
    },
    {
        "MP": formula_parser.parse("A, A > B |- B")
    }
)
command_parser = CommandParser(p1calculus)


@pytest.mark.parametrize(
    ["formula", "render"],
    [
        ["|- p > q", "|- p > q"],
        ["A, A > B |- B", "A, A > B |- B"]
    ]
)
def test_formula_parser(formula, render):
    assert str(formula_parser.parse(formula)) == render


@pytest.mark.parametrize(
    ["formula", "render"],
    [
        ["MP(K, S)", "MP(K, S)"],
        ["[s/p]K", "[s/p]K"],
        ["MP([p>q/q]S, [~p/p]En)", "MP([p > q/q]S, [~p/p]En)"]
    ]
)
def test_command_parser(formula, render):
    assert str(command_parser.parse(formula)) == render
