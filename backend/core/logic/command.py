from __future__ import annotations

from abc import ABC, abstractmethod
from typing import List, Tuple, Dict
from core.logic.formula import Atom, Formula, Expression, Variable, UnaryOperation, BinaryOperation
from core.logic.calculus import Calculus


class Command(ABC):
    @abstractmethod
    def apply(self, calculus: Calculus, steps: List[Formula]) -> Formula:
        pass


class Alias(Command, Atom, ABC):
    def __init__(self, alias: str):
        super().__init__(alias)


class StepNumber(Alias):
    def __init__(self, number: int):
        super().__init__(str(number))

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return isinstance(other, StepNumber) and self.name == other.name

    def apply(self, calculus: Calculus, steps: List[Formula]) -> Formula:
        return steps[int(self.name) - 1]


class Axiom(Alias):
    def __init__(self, name: str):
        super().__init__(name)

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return isinstance(other, Axiom) and self.name == other.name

    def apply(self, calculus: Calculus, steps: List[Formula]) -> Formula:
        return calculus.axioms[self.name]


class InferenceRule(Command):
    def __init__(self, name: str, args: List[Command]):
        self.__name = name
        self.__args = args

    @property
    def name(self):
        return self.__name

    @property
    def args(self):
        return self.__args

    def __str__(self):
        return f"{self.name}({', '.join([str(arg) for arg in self.args])})"

    def apply(self, calculus: Calculus, steps: List[Formula]) -> Formula:
        valuations: Dict[Variable, List[Expression]] = {}

        def valuate_variables(ir_hyp: Expression, ir_arg: Expression):
            if isinstance(ir_hyp, Variable):
                if ir_hyp in valuations:
                    valuations[ir_hyp].append(ir_arg)
                else:
                    valuations[ir_hyp] = [ir_arg]
            elif isinstance(ir_hyp, UnaryOperation) and \
                    isinstance(ir_arg, UnaryOperation) and \
                    type(ir_hyp) == type(ir_arg):
                valuate_variables(ir_hyp.expr, ir_arg.expr)
            elif isinstance(ir_hyp, BinaryOperation) and \
                    isinstance(ir_arg, BinaryOperation) and \
                    type(ir_hyp) == type(ir_arg):
                valuate_variables(ir_hyp.left, ir_arg.left)
                valuate_variables(ir_hyp.right, ir_arg.right)
            else:
                raise Exception("cannot match variables")

        def check_variables():
            for var, expressions in valuations.items():
                es = [str(e) for e in expressions]
                all_equal = es.count(es[0]) == len(es)
                if not all_equal:
                    raise Exception(f"there is unequal valuations for variable {var}")

        ir = calculus.inference_rules[self.name]
        assert len(ir.hypothesis) == len(self.args)
        args = [arg.apply(calculus, steps) for arg in self.args]
        args_hyp = list(dict.fromkeys([hyp for arg in args for hyp in arg.hypothesis]))
        for entry in zip(ir.hypothesis, args):
            valuate_variables(entry[0], entry[1].target)
        check_variables()
        subs = [(value[0], key) for key, value in valuations.items()]
        return Formula(args_hyp, ir.target.substitute(subs))


class SubstitutionCommand(Command):
    def __init__(self, subs: List[Tuple[Expression, Variable]], target: Command):
        self.__subs = subs
        self.__target = target

    @property
    def subs(self):
        return self.__subs

    @property
    def target(self):
        return self.__target

    def __str__(self):
        if isinstance(self.target, Atom):
            return "[" + ", ".join([f"{sub[0]}/{sub[1]}" for sub in self.subs]) + f"]{self.target}"
        return "[" + ", ".join([f"{sub[0]}/{sub[1]}" for sub in self.subs]) + f"]({self.target})"

    def __repr__(self):
        return self.__str__()

    def apply(self, calculus: Calculus, steps: List[Formula]) -> Formula:
        return self.target.apply(calculus, steps).substitute(self.subs)
