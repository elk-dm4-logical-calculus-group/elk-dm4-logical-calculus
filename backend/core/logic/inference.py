from typing import List
from core.logic.command import Command
from core.logic.formula import Formula
from core.logic.calculus import Calculus


class Proof:
    def __init__(self, calculus: Calculus, steps: List[Formula]):
        self.__calculus = calculus
        self.__steps = steps

    @property
    def calculus(self):
        return self.__calculus

    @property
    def steps(self):
        return self.__steps

    def apply(self, command: Command) -> Formula:
        return command.apply(self.calculus, self.steps)
