from typing import Dict
from core.logic.formula import Formula


class Calculus:
    def __init__(
            self,
            axioms: Dict[str, Formula],
            inference_rules: Dict[str, Formula]
    ):
        self.__axioms = axioms
        self.__inference_rules = inference_rules

    @property
    def axioms(self) -> Dict[str, Formula]:
        return self.__axioms

    @property
    def inference_rules(self) -> Dict[str, Formula]:
        return self.__inference_rules
