from __future__ import annotations

from abc import ABC
from typing import List, Tuple


class Expression(ABC):
    def substitute(self, subs: List[Tuple[Expression, Expression]]) -> Expression:
        for sub in subs:
            key = sub[1]
            value = sub[0]
            if self == key:
                return value
        if isinstance(self, Atom):
            return self
        if isinstance(self, UnaryOperation):
            return type(self)(self.expr.substitute(subs))
        if isinstance(self, BinaryOperation):
            return type(self)(self.left.substitute(subs), self.right.substitute(subs))
        if isinstance(self, Substitution):
            raise NotImplemented
        raise Exception("unreachable section")


class Formula:
    def __init__(self, hypothesis: List[Expression], target: Expression):
        self.__hypothesis = hypothesis
        self.__target = target

    @property
    def hypothesis(self) -> List[Expression]:
        return self.__hypothesis

    @property
    def target(self) -> Expression:
        return self.__target

    def __str__(self):
        d = " " if len(self.hypothesis) > 0 else ""
        return ", ".join([str(h) for h in self.hypothesis]) + f"{d}|- {self.target}"

    def __repr__(self):
        return self.__str__()

    def substitute(self, subs: List[Tuple[Expression, Expression]]):
        return Formula(
            [h.substitute(subs) for h in self.hypothesis],
            self.target.substitute(subs)
        )


class Atom(Expression, ABC):
    def __init__(self, name):
        super().__init__()
        self.__name = name

    @property
    def name(self):
        return self.__name


class Variable(Atom):
    def __init__(self, name: str):
        super().__init__(name)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return isinstance(other, Variable) and self.name == other.name

    def __hash__(self):
        return hash(f"{self.name}{type(self).__name__}")


class Constant(Atom):
    def __init__(self, name: str):
        super().__init__(name)

    def __str__(self):
        return f"{self.name}"

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return isinstance(other, Constant) and self.name == other.name


class UnaryOperation(Expression, ABC):
    def __init__(self, expr: Expression):
        super().__init__()
        self.__expr = expr

    @property
    def expr(self):
        return self.__expr


class Inversion(UnaryOperation):
    def __init__(self, expr: Expression):
        super().__init__(expr)

    def __str__(self):
        if isinstance(self.expr, Atom):
            return f"~{self.expr}"
        return f"~({self.expr})"

    def __repr__(self):
        return self.__str__()


class BinaryOperation(Expression, ABC):
    def __init__(self, left: Expression, right: Expression):
        super().__init__()
        self.__left = left
        self.__right = right

    @property
    def left(self):
        return self.__left

    @property
    def right(self):
        return self.__right


class Supset(BinaryOperation):
    def __init__(self, left: Expression, right: Expression):
        super().__init__(left, right)

    def __str__(self):
        if isinstance(self.right, Atom):
            return f"{self.left} > {self.right}"
        return f"{self.left} > ({self.right})"

    def __repr__(self):
        return self.__str__()


class SupsetRev(BinaryOperation):
    def __init__(self, left: Expression, right: Expression):
        super().__init__(left, right)

    def __str__(self):
        if isinstance(self.right, Atom):
            return f"{self.left} < {self.right}"
        return f"{self.left} < ({self.right})"

    def __repr__(self):
        return self.__str__()


class Disjunction(BinaryOperation):
    def __init__(self, left: Expression, right: Expression):
        super().__init__(left, right)

    def __str__(self):
        if isinstance(self.right, Atom):
            return f"{self.left} | {self.right}"
        return f"{self.left} | ({self.right})"

    def __repr__(self):
        return self.__str__()


class Conjunction(BinaryOperation):
    def __init__(self, left: Expression, right: Expression):
        super().__init__(left, right)

    def __str__(self):
        if isinstance(self.right, Atom):
            return f"{self.left} & {self.right}"
        return f"{self.left} & ({self.right})"

    def __repr__(self):
        return self.__str__()


class Equivalence(BinaryOperation):
    def __init__(self, left: Expression, right: Expression):
        super().__init__(left, right)

    def __str__(self):
        if isinstance(self.right, Atom):
            return f"{self.left} == {self.right}"
        return f"{self.left} == ({self.right})"

    def __repr__(self):
        return self.__str__()


class Substitution(Expression):
    def __init__(self, subs: List[Tuple[Expression, Variable]], target: Expression):
        self.__subs = subs
        self.__target = target

    @property
    def subs(self):
        return self.__subs

    @property
    def target(self):
        return self.__target

    def __str__(self):
        if isinstance(self.target, Atom):
            return "[" + ", ".join([f"{sub[0]}/{sub[1]}" for sub in self.subs]) + f"]{self.target}"
        return "[" + ", ".join([f"{sub[0]}/{sub[1]}" for sub in self.subs]) + f"]({self.target})"

    def __repr__(self):
        return self.__str__()
