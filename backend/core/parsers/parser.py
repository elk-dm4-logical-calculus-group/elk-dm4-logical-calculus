import rply
from typing import TypeVar, Generic
from rest_framework import exceptions

T = TypeVar('T')


class Parser(Generic[T]):
    def __init__(
            self,
            tokens_map: dict,
            ignore_list: list,
            precedence: list,
            production_schema: list
    ):
        self.__lexer_gen = rply.LexerGenerator()
        for token_name in tokens_map:
            self.__lexer_gen.add(token_name, tokens_map[token_name])
        for ignorable in ignore_list:
            self.__lexer_gen.ignore(ignorable)
        self.__lexer = self.__lexer_gen.build()

        self.__parser_gen = rply.ParserGenerator(
            list(tokens_map.keys()),
            precedence
        )

        if production_schema is None:
            return
        for prod_rules_subset in production_schema:
            if "precedences" not in prod_rules_subset:
                prod_rules_subset["precedences"] = None
            self.__add_parser_production(
                prod_rules_subset["func"],
                prod_rules_subset["rules"],
                prod_rules_subset["precedences"]
            )
        self.__parser = self.__parser_gen.build()

    def __add_parser_production(self, func: "token(iterable) -> Any", rules: [str], precedences: list = None) -> None:
        assert (precedences is None or len(rules) == len(precedences)) and len(rules) > 0
        if precedences is None:
            precedences = [None] * len(rules)

        if len(rules) == 1:
            self.__parser_gen.production(rules[0], precedences[0])(func)
        else:
            self.__add_parser_production(
                self.__parser_gen.production(rules[len(rules) - 1], precedences[len(precedences) - 1])(func),
                rules[:len(rules) - 1],
                precedences[:len(precedences) - 1]
            )

    def parse(self, text: str) -> T:
        return self.__parser.parse(self.__lexer.lex(text))

    def parse_validate(self, text: str) -> T:
        try:
            return self.parse(text)
        except rply.errors.ParsingError as p:
            raise exceptions.ValidationError(detail=f'Ошибка парсинга выражения "{text}": {p}')
        except rply.errors.LexingError as l:
            raise exceptions.ValidationError(detail=f'Ошибка парсинга выражения "{text}": {l}')
