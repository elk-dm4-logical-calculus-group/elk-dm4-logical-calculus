from core.parsers.parser import Parser
from core.logic.formula import *


class FormulaParser(Parser[Formula]):
    __tokens_map = {
        "OPEN_PARENS": r'\(',
        "CLOSE_PARENS": r'\)',
        "COMA": r',',
        "TURNSTILE": r'\|-',
        "SQR_OPEN_PARENS": r'\[',
        "SQR_CLOSE_PARENS": r'\]',
        "SUBSTITUTION_SIGN": r'/',
        "SUPSET": r'>',
        "SUPSET_REV": r'<',
        "INVERSION": r'~',
        "OR": r'\|',
        "AND": r'&',
        "EQUIVALENCE": r'==',
        "CONST": r'\b[ft]\b',
        "VAR": r'[a-zA-Z_][\w_]*'
    }

    __ignore = [r'\s+']

    __precedence = [
        ("left", ["TURNSTILE"]),
        ("left", ["COMA"]),
        ("right", ["SQR_OPEN_PARENS", "SQR_CLOSE_PARENS", "SUBSTITUTION_SIGN"]),

        ("left", ["EQUIVALENCE"]),
        ("left", ["SUPSET", "SUPSET_REV"]),
        ("left", ["OR"]),
        ("left", ["AND"]),

        ("left", ["OPEN_PARENS", "CLOSE_PARENS"]),
        ("left", ["INVERSION"]),
        ("left", ["CONST", "VAR"])
    ]

    __production_schema = [
        {
            "func": lambda t: Formula(t[0], t[2]),
            "rules": ['formula : hypotheses TURNSTILE expression'],
            "precedences": None
        },
        {
            "func": lambda t: Formula([], t[1]),
            "rules": ['formula : TURNSTILE expression'],
            "precedences": None
        },
        {
            "func": lambda t: [*t[0], t[2]],
            "rules": ['hypotheses : hypotheses COMA expression'],
            "precedences": None
        },
        {
            "func": lambda t: [t[0]],
            "rules": ['hypotheses : expression'],
            "precedences": None
        },
        {
            "func": lambda t: Substitution(t[1], t[3]),
            "rules": ['expression : SQR_OPEN_PARENS substitution_rule SQR_CLOSE_PARENS expression'],
            "precedences": None
        },
        {
            "func": lambda t: [(t[0], t[2])],
            "rules": ['substitution_rule : expression SUBSTITUTION_SIGN VAR']
        },
        {
            "func": lambda t: [*t[0], (t[2], t[4])],
            "rules": ['substitution_rule : substitution_rule COMA expression SUBSTITUTION_SIGN VAR']
        },
        {
            "func": lambda t: Constant(t[0].getstr()),
            "rules": ['expression : CONST'],
            "precedences": None
        },
        {
            "func": lambda t: Variable(t[0].getstr()),
            "rules": ['expression : VAR'],
            "precedences": None
        },
        {
            "func": lambda t: t[1],
            "rules": ['expression : OPEN_PARENS expression CLOSE_PARENS'],
            "precedences": None
        },
        {
            "func": lambda t: Supset(t[0], t[2]),
            "rules": ['expression : expression SUPSET expression'],
            "precedences": None
        },
        {
            "func": lambda t: SupsetRev(t[0], t[2]),
            "rules": ['expression : expression SUPSET_REV expression'],
            "precedences": None
        },
        {
            "func": lambda t: Inversion(t[1]),
            "rules": ['expression : INVERSION expression'],
            "precedences": None
        },
        {
            "func": lambda t: Disjunction(t[0], t[2]),
            "rules": ['expression : expression OR expression'],
            "precedences": None
        },
        {
            "func": lambda t: Conjunction(t[0], t[2]),
            "rules": ['expression : expression AND expression'],
            "precedences": None
        },
        {
            "func": lambda t: Equivalence(t[0], t[2]),
            "rules": ['expression : expression EQUIVALENCE expression'],
            "precedences": None
        }
    ]

    def __init__(self):
        super().__init__(
            FormulaParser.__tokens_map,
            FormulaParser.__ignore,
            FormulaParser.__precedence,
            FormulaParser.__production_schema
        )
