import copy
from core.parsers.parser import Parser
from core.logic.formula import Constant, Variable, Supset, SupsetRev, Disjunction, Conjunction, \
    Equivalence, Inversion
from core.logic.command import Axiom, InferenceRule, StepNumber, SubstitutionCommand, Command
from core.logic.calculus import Calculus


class CommandParser(Parser[Command]):
    __tokens_map = {
        "OPEN_PARENS": r'\(',
        "CLOSE_PARENS": r'\)',
        "COMA": r',',
        "SQR_OPEN_PARENS": r'\[',
        "SQR_CLOSE_PARENS": r'\]',
        "SUBSTITUTION_SIGN": r'/',
        "SUPSET": r'>',
        "SUPSET_REV": r'<',
        "INVERSION": r'~',
        "OR": r'\|',
        "AND": r'&',
        "EQUIVALENCE": r'==',
        "AXIOM": None,
        "INFERENCE_RULE": None,
        "EXPR_NUM": r'\d+',
        "CONST": r'\b[ft]\b',
        "VAR": r'[a-zA-Z_][\w_]*'
    }
    __ignore = [r'\s+']
    __precedence = [
        ("left", ["INFERENCE_RULE"]),
        ("left", ["COMA"]),
        ("right", ["SQR_OPEN_PARENS", "SQR_CLOSE_PARENS", "SUBSTITUTION_SIGN"]),

        ("left", ["EQUIVALENCE"]),
        ("left", ["SUPSET", "SUPSET_REV"]),
        ("left", ["OR"]),
        ("left", ["AND"]),

        ("left", ["OPEN_PARENS", "CLOSE_PARENS"]),
        ("left", ["INVERSION"]),
        ("left", ["CONST", "VAR", "EXPR_NUM", "AXIOM"])
    ]
    __production_schema = [
        {
            "func": lambda t: Axiom(t[0].getstr()),
            "rules": ['command : AXIOM'], "precedences": None
        },
        {
            "func": lambda t: t[0],
            "rules": ['command : substitution'], "precedences": None
        },
        {
            "func": lambda t: InferenceRule(t[0].getstr(), t[1]),
            "rules": ['command : INFERENCE_RULE args_complete'], "precedences": None
        },
        {
            "func": lambda t: [],
            "rules": ['args_complete : OPEN_PARENS CLOSE_PARENS'], "precedences": None
        },
        {
            "func": lambda t: t[1],
            "rules": ['args_complete : OPEN_PARENS args CLOSE_PARENS'], "precedences": None
        },
        {
            "func": lambda t: [t[0]],
            "rules": ['args : argument'], "precedences": None
        },
        {
            "func": lambda t: [*t[0], t[2]],
            "rules": ['args : args COMA argument'], "precedences": None
        },
        {
            "func": lambda t: t[0],
            "rules": ['argument : expression'], "precedences": None
        },
        {
            "func": lambda t: StepNumber(int(t[0].getstr())),
            "rules": ['argument : EXPR_NUM'], "precedences": None
        },
        {
            "func": lambda t: t[0],
            "rules": ['argument : command'], "precedences": None
        },
        {
            "func": lambda t: SubstitutionCommand(t[1], t[3]),
            "rules": ['substitution : SQR_OPEN_PARENS substitution_rule SQR_CLOSE_PARENS argument'],
            "precedences": None
        },
        {
            "func": lambda t: [(t[0], Variable(t[2].getstr()))],
            "rules": ['substitution_rule : expression SUBSTITUTION_SIGN VAR']
        },
        {
            "func": lambda t: [*t[0], (t[2], Variable(t[4].getstr()))],
            "rules": ['substitution_rule : substitution_rule COMA expression SUBSTITUTION_SIGN VAR']
        },
        {
            "func": lambda t: Constant(t[0].getstr()),
            "rules": ['expression : CONST'], "precedences": None
        },
        {
            "func": lambda t: Variable(t[0].getstr()),
            "rules": ['expression : VAR'], "precedences": None
        },
        {
            "func": lambda t: t[1],
            "rules": ['expression : OPEN_PARENS expression CLOSE_PARENS'], "precedences": None
        },
        {
            "func": lambda t: Supset(t[0], t[2]),
            "rules": ['expression : expression SUPSET expression'], "precedences": None
        },
        {
            "func": lambda t: SupsetRev(t[0], t[2]),
            "rules": ['expression : expression SUPSET_REV expression'], "precedences": None
        },
        {
            "func": lambda t: Inversion(t[1]),
            "rules": ['expression : INVERSION expression'], "precedences": None
        },
        {
            "func": lambda t: Disjunction(t[0], t[2]),
            "rules": ['expression : expression OR expression'], "precedences": None
        },
        {
            "func": lambda t: Conjunction(t[0], t[2]),
            "rules": ['expression : expression AND expression'], "precedences": None
        },
        {
            "func": lambda t: Equivalence(t[0], t[2]),
            "rules": ['expression : expression EQUIVALENCE expression'], "precedences": None
        }
    ]

    def __init__(self, calculus: Calculus):
        tokens_map = copy.deepcopy(CommandParser.__tokens_map)
        tokens_map["AXIOM"] = r"|".join(list(map(lambda s: rf"\b{s}\b", calculus.axioms.keys())))
        tokens_map["INFERENCE_RULE"] = r"|".join(list(map(lambda s: rf"\b{s}\b", calculus.inference_rules.keys())))
        super().__init__(
            tokens_map,
            CommandParser.__ignore,
            CommandParser.__precedence,
            CommandParser.__production_schema
        )
