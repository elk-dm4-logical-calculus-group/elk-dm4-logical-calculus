from api import utils
from rest_framework.decorators import action
from rest_framework.response import Response
from core import serializers
from rest_framework import permissions, authentication

# Create your views here.


class InverenceViewSet(utils.SAGenericViewSet):
    serializer_classes = {
        'make_inference': serializers.InferenceSerializer
    }

    action_permission_classes = {
        'default': [permissions.IsAuthenticated]
    }

    authentication_classes = [authentication.TokenAuthentication]

    @action(methods=['post'], detail=False)
    def make_inference(self, request, *args, **kwargs):
        user = request.user
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(data=serializer.data)
