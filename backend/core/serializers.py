from rest_framework import serializers, exceptions
from core.parsers import command_parser, formula_parser
from core.logic import calculus, command, formula, inference

class NamedFormulaSerializer(serializers.Serializer):
    name = serializers.CharField()
    notation = serializers.CharField()

class StepSerializer(serializers.Serializer):
    formula = serializers.CharField()
    command = serializers.CharField()

class InferenceSerializer(serializers.Serializer):
    command = serializers.CharField(write_only=True)
    axioms = NamedFormulaSerializer(many=True, write_only=True)
    rules = NamedFormulaSerializer(many=True, write_only=True)
    steps = StepSerializer(write_only=True, many=True)
    formula = serializers.CharField(read_only=True)

    def validate(self, attrs):
        axioms = {a['name']: formula_parser.FormulaParser().parse_validate(a['notation']) for a in attrs.get('axioms')}
        inference_rules = {r['name']: formula_parser.FormulaParser().parse_validate(r['notation']) for r in attrs.get('rules')}
        calculus = inference.Calculus(axioms, inference_rules)
        command = command_parser.CommandParser(calculus=calculus).parse_validate(attrs.get('command'))
        steps = [formula_parser.FormulaParser().parse_validate(s['formula']) for s in attrs.get('steps')]
        try:
            proof = inference.Proof(calculus, steps)
            formula = proof.apply(command)
        except Exception as e:
            raise exceptions.ValidationError(detail=f"Ошибка вывода по команде {command}: {e}")
        attrs['formula'] = str(formula)
        return attrs
