from django.views.generic.base import RedirectView
from django.views.generic import TemplateView


class IndexView(TemplateView):
    template_name = 'index.html'
