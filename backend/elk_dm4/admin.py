from django.contrib import admin
from rest_framework.authtoken.admin import User


class UserAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)
