from rest_framework import serializers
from inference import models
from api.serializers import DefaultUserSerializer


class DefaultInferenceRuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.InferenceRule
        fields = '__all__'


class DefaultCalculusSerializer(serializers.ModelSerializer):
    inference_rules = DefaultInferenceRuleSerializer(read_only=True, many=True)
    class Meta:
        model = models.Calculus
        fields = '__all__'


class RetrieveCalculusSerializer(serializers.ModelSerializer):
    inference_rules = DefaultInferenceRuleSerializer(read_only=True, many=True)

    class Meta:
        model = models.Calculus
        fields = '__all__'


class DefaultFormulaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Formula
        fields = '__all__'


class DefaultProofStepSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProofStep
        fields = '__all__'


class DefaultProofSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Proof
        fields = '__all__'


class RetrieveProofStepSerializer(serializers.ModelSerializer):
    proof = DefaultProofSerializer(read_only=True, many=False)

    class Meta:
        model = models.ProofStep
        fields = '__all__'


class RetrieveProofSerializer(serializers.ModelSerializer):
    formula = DefaultFormulaSerializer(read_only=True, many=False)
    users = DefaultUserSerializer(read_only=True, many=True)
    proof_steps = DefaultProofStepSerializer(read_only=True, many=True, source='proofstep_set')
    class Meta:
        model = models.Proof
        fields = '__all__'