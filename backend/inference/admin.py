from django.contrib import admin
from inference import models

# Register your models here.


@admin.register(models.Calculus)
class CalculusAdmin(admin.ModelAdmin):
    list_display = 'id', 'name'
    search_fields = list_display


@admin.register(models.Formula)
class FormulaAdmin(admin.ModelAdmin):
    list_display = 'id', 'name', 'notation'
    search_fields = list_display


@admin.register(models.InferenceRule)
class InferenceRuleAdmin(admin.ModelAdmin):
    list_display = 'id', 'name', 'notation', 'is_axiom'
    search_fields = 'id', 'name', 'notation'
    list_filter = 'is_axiom',


@admin.register(models.Proof)
class ProofAdmin(admin.ModelAdmin):
    list_display = 'id', 'formula', 'notation_formula'
    search_fields = list_display


@admin.register(models.ProofStep)
class ProofStepAdmin(admin.ModelAdmin):
    list_display = 'id', 'ordinal_num', 'applied_rule'
    search_fields = list_display
