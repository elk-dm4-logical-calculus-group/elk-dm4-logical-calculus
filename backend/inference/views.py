from api import utils
from inference import models, serializers
from rest_framework import permissions, authentication


# Create your views here.


class InferenceRuleViewSet(utils.SAModelViewSet):
    queryset = models.InferenceRule.objects.all()

    serializer_classes = {
        'default': serializers.DefaultInferenceRuleSerializer
    }
    action_permission_classes = {
        'default': [permissions.IsAuthenticated]
    }
    authentication_classes = [authentication.TokenAuthentication]


class CalculusViewSet(utils.SAModelViewSet):
    queryset = models.Calculus.objects.all()

    serializer_classes = {
        'default': serializers.DefaultCalculusSerializer,
        'retrieve': serializers.RetrieveCalculusSerializer
    }
    action_permission_classes = {
        'default': [permissions.IsAuthenticated]
    }
    authentication_classes = [authentication.TokenAuthentication]


class FormulaViewSet(utils.SAModelViewSet):
    queryset = models.Formula.objects.all()

    serializer_classes = {
        'default': serializers.DefaultFormulaSerializer
    }
    action_permission_classes = {
        'default': [permissions.IsAuthenticated]
    }
    authentication_classes = [authentication.TokenAuthentication]


class ProofViewSet(utils.SAModelViewSet):
    queryset = models.Proof.objects.all()

    serializer_classes = {
        'default': serializers.DefaultProofSerializer,
        'retrieve': serializers.RetrieveProofSerializer
    }
    action_permission_classes = {
        'default': [permissions.IsAuthenticated]
    }
    authentication_classes = [authentication.TokenAuthentication]


class ProofStepViewSet(utils.SAModelViewSet):
    queryset = models.ProofStep.objects.all()

    serializer_classes = {
        'default': serializers.DefaultProofStepSerializer,
        'retrieve': serializers.RetrieveProofStepSerializer
    }
    action_permission_classes = {
        'default': [permissions.IsAuthenticated]
    }
    authentication_classes = [authentication.TokenAuthentication]
