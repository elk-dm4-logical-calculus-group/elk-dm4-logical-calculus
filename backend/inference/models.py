from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


def nstr(obj) -> str:
    return "<null>" if obj is None else obj.__str__()


class InferenceRule(models.Model):
    name = models.CharField(max_length=32, null=False)
    notation = models.CharField(max_length=256, null=False)
    is_axiom = models.BooleanField(null=False)

    def __str__(self):
        return f"{self.__class__.__name__}: {{id:{nstr(self.id)}; name:{nstr(self.name)}; notation:{nstr(self.notation)}; is_axiom:{nstr(self.is_axiom)}}}"


class Calculus(models.Model):
    name = models.CharField(max_length=64, null=False)
    inference_rules = models.ManyToManyField(InferenceRule)

    def __str__(self):
        return f"{self.__class__.__name__}: {{id:{nstr(self.id)}; name:{nstr(self.name)}; inference_rules:[{', '.join([nstr(obj.id) for obj in self.inference_rules.all()])}]}}"


class Formula(models.Model):
    name = models.CharField(max_length=32, null=False)
    notation = models.CharField(max_length=512, null=False)

    def __str__(self):
        return f"{self.__class__.__name__}: {{id:{nstr(self.id)}; name:{nstr(self.name)}; notation:{nstr(self.notation)}}}"


class Proof(models.Model):
    formula = models.ForeignKey(Formula, on_delete=models.CASCADE, null=True, blank=True)
    notation_formula = models.CharField(max_length=512, null=True, blank=True)
    users = models.ManyToManyField(User)

    def save(self, *args, **kwargs):
        if (not self.formula) and (not self.notation_formula):
            raise ValidationError("Proof fields formula and notation_formula can not be set to NULL simultaneously")
        super().save(*args, **kwargs)

    def __str__(self):
        if self.formula:
            return f"{self.__class__.__name__}: {{id:{nstr(self.id)}; formula:{nstr(self.formula.id)}; notation_formula:{nstr(self.notation_formula)}, users:[{', '.join([nstr(obj.id) for obj in self.users.all()])}]}}"
        else:
            return f"{self.__class__.__name__}: {{id:{nstr(self.id)}; formula:<null>; notation_formula:{nstr(self.notation_formula)}, users:[{', '.join([nstr(obj.id) for obj in self.users.all()])}]}}"


class ProofStep(models.Model):
    proof = models.ForeignKey(Proof, on_delete=models.CASCADE, null=False)
    ordinal_num = models.PositiveSmallIntegerField(null=False)
    applied_rule = models.CharField(max_length=32, null=False)

    def __str__(self):
        if self.proof:
            return f"{self.__class__.__name__}: {{id:{nstr(self.id)}; proof:{nstr(self.proof.id)}; ordinal_num:{nstr(self.ordinal_num)}; applied_rule:{nstr(self.applied_rule)}}}"
        else:
            return f"{self.__class__.__name__}: {{id:{nstr(self.id)}; proof:<null>; ordinal_num:{nstr(self.ordinal_num)}; applied_rule:{nstr(self.applied_rule)}}}"

    class Meta:
        indexes = [
            models.Index(fields=['ordinal_num']),
            models.Index(fields=['proof']),
        ]
