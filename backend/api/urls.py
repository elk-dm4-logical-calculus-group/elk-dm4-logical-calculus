from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework import permissions
from drf_spectacular.views import SpectacularRedocView, SpectacularSwaggerView

from api import views
from core import views as core_views
from inference import views as inference_views

router = DefaultRouter()
# сюда добавляем router.register(#PATH, #VIEWSET, basename=#NAME)
router.register(r'users', views.UserViewSet, basename='users')
router.register(r'inference_rules', inference_views.InferenceRuleViewSet, basename='inference_rules')
router.register(r'calculus', inference_views.CalculusViewSet, basename='calculus')
router.register(r'formulas', inference_views.FormulaViewSet, basename='formulas')
router.register(r'proofs', inference_views.ProofViewSet, basename='proofs')
router.register(r'proof_steps', inference_views.ProofStepViewSet, basename='proof_steps')
router.register(r'core', core_views.InverenceViewSet, basename='core')

urlpatterns = [
    path('schema/', views.SpectacularHAPIView.as_view(), name='schema'),
    # Optional UI:
    path('swagger/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),
    path('', include(router.urls))
]
