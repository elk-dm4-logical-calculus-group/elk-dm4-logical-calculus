from rest_framework import filters, serializers, viewsets


class SAGenericViewSet(viewsets.GenericViewSet):
    serializer_classes = {}
    action_permission_classes = {}

    def get_serializer_class(self):
        action = self.action if self.action in self.serializer_classes else 'default' if 'default' in self.serializer_classes else None
        if action is not None:
            return self.serializer_classes[action]
        return super().get_serializer_class()

    def get_permissions(self):
        action = self.action if self.action in self.action_permission_classes else 'default' if 'default' in self.action_permission_classes else None
        if action is not None:
            return [permission() for permission in self.action_permission_classes[action]] if isinstance(self.action_permission_classes[action], list) else [self.action_permission_classes[action]()]
        return super().get_permissions()

class SAModelViewSet(viewsets.ModelViewSet):
    serializer_classes = {}
    action_permission_classes = {}

    def get_serializer_class(self):
        action = self.action if self.action in self.serializer_classes else 'default' if 'default' in self.serializer_classes else None
        if action is not None:
            return self.serializer_classes[action]
        return super().get_serializer_class()

    def get_permissions(self):
        action = self.action if self.action in self.action_permission_classes else 'default' if 'default' in self.action_permission_classes else None
        if action is not None:
            return [permission() for permission in self.action_permission_classes[action]] if isinstance(self.action_permission_classes[action], list) else [self.action_permission_classes[action]()]
        return super().get_permissions()

class SAReadOnlyModelViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_classes = {}
    action_permission_classes = {}

    def get_serializer_class(self):
        action = self.action if self.action in self.serializer_classes else 'default' if 'default' in self.serializer_classes else None
        if action is not None:
            return self.serializer_classes[action]
        return super().get_serializer_class()

    def get_permissions(self):
        action = self.action if self.action in self.action_permission_classes else 'default' if 'default' in self.action_permission_classes else None
        if action is not None:
            return [permission() for permission in self.action_permission_classes[action]] if isinstance(self.action_permission_classes[action], list) else [self.action_permission_classes[action]()]
        return super().get_permissions()