from django.shortcuts import render
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.authentication import TokenAuthentication
from drf_spectacular.views import SpectacularAPIView

from django.contrib.auth.models import User

from api import utils, serializers

# Create your views here.


class UserViewSet(utils.SAReadOnlyModelViewSet):
    queryset = User.objects.all()

    serializer_classes = {
        'default': serializers.DefaultUserSerializer,
        'sign_in': serializers.SignInUserSerializer
    }
    action_permission_classes = {
        'default': [permissions.IsAuthenticated],
        'sign_in': [permissions.AllowAny]
    }
    authentication_classes = [TokenAuthentication]

    @action(methods=['POST'], detail=False)
    def sign_in(self, request, *args, **kwargs):
        # сериалайзер достанется из словаря serializer_classes по полю 'sign_in'
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(data=serializer.data)


class SpectacularHAPIView(SpectacularAPIView):
    def _get_schema_response(self, request):
        generator = self.generator_class(urlconf=self.urlconf, api_version=self.api_version)
        res = generator.get_schema(request=request, public=self.serve_public)
        main_host = {'url':f"{request.META['wsgi.url_scheme']}://{request.META['HTTP_HOST']}", 'description': 'Main host'}
        res['servers'] = [main_host] if 'servers' not in res else [main_host] + res['servers']
        return Response(res)
