
from rest_framework import serializers
from rest_framework.response import Response

from django.contrib.auth.models import User
from django.utils import timezone
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer

class DefaultUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'last_login',
            'first_name',
            'last_name',
            'email',
            'date_joined'
        ]

class SignInUserSerializer(AuthTokenSerializer):
    def validate(self, attrs):
        attrs = super().validate(attrs)
        user = attrs['user']
        attrs['token'], created = Token.objects.get_or_create(user=user)
        user.last_login = timezone.now()
        user.save()
        return attrs